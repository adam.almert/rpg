
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include "other.h"
#include "Templates.h"
#include "Hero.h"
#include "Monsters.h"

using namespace std;



int main()
{
    srand((unsigned int)time(NULL));
    int choice, f_result;
    int s, d, e, i, c;
    int CharacterStrength = 0, CharacterDexterity = 0, CharacterEndurance = 0, CharacterIntelligence, CharacterCharisma, CharacterExperience, CharacterLevel;
    int MonstersStrength = 0, MonstersEndurance = 0, MonstersExperience = 0;
    string n, monsters_team, CharacterName, MonstersName;
    character chr(1);
    monsters m(1);
    interface* temp;
    
    

    do
    {
	cout << "What do you want to do?" << endl;
	cout << "1 - Save a new character" << endl;
	cout << "2 - Load a character" << endl;
	cout << "3 - Generate a team of monsters" << endl;
	cout << "4 - Load a team of monsters" << endl;
	cout << "5 - Fight " << endl;
	cout << "6 - Quit " << endl;
	cin >> choice;


	if (choice == 1)
	{
	    cout << "Enter the name of your new character : ";
	    cin >> n;
	    cout << "Strength : ";
	    cin >> s;
	    cout << "Dexterity : ";
	    cin >> d;
	    cout << "Endurance : ";
	    cin >> e;
	    cout << "Intelligence : ";
	    cin >> i;
	    cout << "Charisma : ";
	    cin >> c;
	    character ch(n, s, d, e, i, c, 0, 1);

	    ch.klasa();
	    ch.save();

	}
	else if (choice == 2)
	{
	    
	    temp = &chr;
	    temp -> load();
	}
	else if (choice == 3)
	{
	    cout << "Enter the name of the new team : ";
	    cin >> monsters_team;
	    monsters m(monsters_team);
	}
	else if (choice == 4)
	{
	    
	    temp = &m;
	    temp -> load();
	}
	else if (choice == 5)
	{
	    cout << "Choose which character and which monsters team you want to fight:" << endl;
	    temp = &chr;
	    temp->load();
	    CharacterName = temp->get_name();
	    CharacterStrength = temp->get_strength();
	    CharacterDexterity = temp->get_dexterity();
	    CharacterEndurance = temp->get_endurance();
	    CharacterIntelligence = temp->get_intelligence();
	    CharacterCharisma = temp->get_charisma();
	    CharacterExperience = temp->get_experience();
	    CharacterLevel = temp->get_level();

	    temp = &m;
	    temp->load();
	    MonstersName = temp->get_name();
	    MonstersStrength = temp->get_strength();
	    MonstersEndurance = temp->get_endurance();
	    MonstersExperience = temp->get_experience();

	    f_result=fight<int>(CharacterStrength, CharacterEndurance, CharacterExperience, MonstersStrength, MonstersEndurance, MonstersExperience);

	    if (f_result > 0)
	    {
		remove(MonstersName.c_str());
	    }

	    CharacterExperience = CharacterExperience + f_result;
	    
	    while (CharacterExperience > CharacterLevel * 100)
	    {
		CharacterExperience = CharacterExperience - CharacterLevel * 100;
		CharacterLevel = CharacterLevel+1;
	    }

	    character ch2(CharacterName, CharacterStrength, CharacterDexterity, CharacterEndurance, CharacterIntelligence, CharacterCharisma, CharacterExperience, CharacterLevel);
	    temp = &ch2;
	    temp->save();

	}
	else if (choice == 6)
	{
	    break;
	}
	else cout << "Incorrect number!!";
	cout << "---------------------------------" << endl;
    } while (true);

    return 0;
}
