#pragma once
#include <iostream>
#include <stdio.h>

using namespace std;

class mage
{
    int i;
public:
    void stat(int intel)
    {
	i = intel + 3;
    }
    friend class character;
};
class warrior
{
    int e;
    void stat(int endur)
    {
	e = endur + 3;
    }
    friend class character;
};
class berserker
{
    int s;
    void stat(int stren)
    {
	s = stren + 3;
    }
    friend class character;
};
class thief
{
    int d;
    void stat(int dexter)
    {
	d = dexter + 3;
    }
    friend class character;
};


class character : public interface
{


public:
    character(int whatever = 1)
    {
    }
    character(string name, int strength, int dexterity, int endurance, int intelligence, int charisma, int experience, int level)
    {
	n = name;
	s = strength;
	d = dexterity;
	e = endurance;
	i = intelligence;
	c = charisma;
	exp = experience;
	lvl = level;

    }
    ~character()
    {
    }




    void klasa()
    {
	int klasa;
	cout << "Choose the class of your character : " << endl;
	cout << "1 - Mage" << endl << "2 - Warrior" << endl << "3 - Berserker" << endl << "4 - Thief" << endl;
	cin >> klasa;
	switch (klasa)
	{
	case 1:
	    mage m;
	    m.stat(i);
	    i = m.i;
	    break;
	case 2:
	    warrior w;
	    w.stat(e);
	    e = w.e;
	    break;
	case 3:
	    berserker b;
	    b.stat(s);
	    s = b.s;
	    break;
	case 4:
	    thief t;
	    t.stat(d);
	    d = t.d;
	    break;
	default:
	    cout << "Wrong number" << endl;
	    break;

	}

    }

    void save()
    {
	remove(n.c_str());
	ofstream file;
	file.open(n.c_str(), ios::app);
	if (file.good() == true)
	{
	    file << s << " " << d << " " << e << " " << i << " " << c << " " << exp << " " << lvl << endl;
	    file.close();
	}
	else cout << "not good" << endl;
    }

    void load()
    {


	cout << "What character do you want to load?" << endl;
	cin >> n;
	fstream file;
	file.open(n.c_str());
	if (file.good() == true)
	{
	    file >> s >> d >> e >> i >> c >> exp >> lvl;
	    cout << "Loaded character '" << n << "' with the following statistics:" << endl;
	    cout << "Strength : " << s << endl;
	    cout << "Dexterity : " << d << endl;
	    cout << "Endurance : " << e << endl;
	    cout << "Intelligence : " << i << endl;
	    cout << "Charisma : " << c << endl;
	    cout << "Experience : " << exp << endl;
	    cout << "Level : " << lvl << endl;
	    file.close();

	}
	else cout << "There is no character with such a name" << endl;

    }

    string get_name()
    {
	return this->n;
    }

    int get_strength()
    {

	return this->s;
    }
    int get_dexterity()
    {

	return this->d;
    }
    int get_endurance()
    {

	return this->e;
    }
    int get_intelligence()
    {

	return this->i;
    }
    int get_charisma()
    {

	return this->c;
    }
    int get_experience()
    {

	return this->exp;
    }
    int get_level()
    {

	return this->lvl;
    }
};