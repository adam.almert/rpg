#pragma once
#include <iostream>
#include <stdio.h>

using namespace std;

class monsters : public interface
{

public:
    monsters(int temp = 0)
    {}
    monsters(string name = "teamA")
    {
	n = name;



	for (int j = 0; j < 5; j++)
	{
	    s = rand() % 10 + 1;
	    d = rand() % 10 + 1;
	    e = rand() % 10 + 1;
	    i = rand() % 10 + 1;
	    c = rand() % 10 + 1;
	    exp = s + d + i + c;
	    save();
	}


    }
    ~monsters()
    {}

    void save()
    {
	ofstream file;
	file.open(n.c_str(), ios::app);
	if (file.good() == true)
	{
	    file << s << " " << d << " " << e << " " << i << " " << c << " " << exp << endl;
	    file.close();
	}
	else cout << "not good" << endl;
    }

    void load()
    {
	CombinedStrength = 0;
	CombinedDexterity = 0;
	CombinedEndurance = 0;
	CombinedExperience = 0;
	CombinedIntelligence = 0;
	CombinedCharisma = 0;


	cout << "Enter the name of the team : ";
	cin >> n;
	fstream file;
	file.open(n.c_str());
	if (file.good() == true)
	{
	    for (int j = 1; j < 6; j++)
	    {
		file >> s >> d >> e >> i >> c >> exp;
		cout << "Loaded '" << j << "' monster from the team '" << n << "' with the following statistics:" << endl;
		cout << "Strength : " << s << endl;
		cout << "Dexterity : " << d << endl;
		cout << "Endurance : " << e << endl;
		cout << "Intelligence : " << i << endl;
		cout << "Charisma : " << c << endl;
		cout << "Experience : " << exp << endl;
		cout << "......................................" << endl;
		CombinedStrength = CombinedStrength + s;
		CombinedEndurance = CombinedEndurance + e;
		CombinedExperience = CombinedExperience + exp;
	    }
	    file.close();
	}
	else cout << "There is no team with such a name" << endl;
    }

    string get_name()
    {
	return this->n;
    }
    int get_strength()
    {

	return this->CombinedStrength;
    }
    int get_dexterity()
    {

	return this->CombinedDexterity;
    }
    int get_endurance()
    {

	return this->CombinedEndurance;
    }
    int get_intelligence()
    {

	return this->CombinedIntelligence;
    }
    int get_charisma()
    {

	return this->CombinedCharisma;
    }
    int get_experience()
    {

	return this->CombinedExperience;
    }
    int get_level()
    {

	return this->lvl;
    }

};

