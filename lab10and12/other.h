#pragma once
#include <iostream>
#include <stdio.h>



using namespace std;



class interface {


protected:
    int s, d, e, i, c, exp, lvl, CombinedStrength, CombinedDexterity, CombinedEndurance, CombinedIntelligence, CombinedCharisma, CombinedExperience;
    string n;
    

public:

    virtual ~interface() {};
    virtual void save() = 0;
    
    virtual void load()=0;
    virtual string get_name() = 0;
    virtual  int get_strength() = 0;
    virtual int get_dexterity() = 0;
    virtual int get_endurance() = 0;
    virtual int get_intelligence() = 0;
    virtual int get_charisma() = 0;
    virtual int get_experience() = 0;
    virtual int get_level() = 0;
};



