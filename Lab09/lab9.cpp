#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include "other.h"

using namespace std;



int main()
{
    srand((unsigned int)time(NULL));
    int choice;
    int s, d, e, i, c;
    string n, monsters_team;

    do
    {
	cout << "What do you want to do?" << endl;
	cout << "1 - Save a new character" << endl;
	cout << "2 - Load a character" << endl;
	cout << "3 - Generate a team of monsters" << endl;
	cout << "4 - Quit" << endl;
	cin >> choice;


	if (choice == 1)
	{
	    cout << "Enter the name of your new character : ";
	    cin >> n;
	    cout << "Strength : ";
	    cin >> s;
	    cout << "Dexterity : ";
	    cin >> d;
	    cout << "Endurance : ";
	    cin >> e;
	    cout << "Intelligence : ";
	    cin >> i;
	    cout << "Charisma : ";
	    cin >> c;
	    character ch(n, s, d, e, i, c);

	    ch.klasa();
	    ch.save();

	}
	else if (choice == 2)
	{
	    load();
	}
	else if (choice == 3)
	{
	    cout << "Enter the name of the new team : ";
	    cin >> monsters_team;
	    monsters m(monsters_team);
	}
	else if (choice == 4)
	{
	    break;
	}
	else cout << "Incorrect number!!";
	cout << "---------------------------------" << endl;
    } while (true);

    return 0;
}