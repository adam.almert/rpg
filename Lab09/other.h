#pragma once
#include <iostream>


using namespace std;



class mage
{
    int i;
public:
    void stat(int intel)
    {
	i = intel + 3;
    }
    friend class character;
};
class warrior
{
    int e;
    void stat(int endur)
    {
	e = endur + 3;
    }
    friend class character;
};
class berserker
{
    int s;
    void stat(int stren)
    {
	s = stren + 3;
    }
    friend class character;
};
class thief
{
    int d;
    void stat(int dexter)
    {
	d = dexter + 3;
    }
    friend class character;
};


class character
{
protected:
    int s, d, e, i, c;
    string n;

public:
    character()
    {}
    character(string name, int strength, int dexterity, int endurance, int intelligence, int charisma)
    {
	n = name;
	s = strength;
	d = dexterity;
	e = endurance;
	i = intelligence;
	c = charisma;

    }
    ~character()
    {
	cout << endl << "zniszczone" << endl;
    }


    void save()
    {
	ofstream file;
	file.open(n.c_str(), ios::app);
	if (file.good() == true)
	{
	    file << s << " " << d << " " << e << " " << i << " " << c << endl;
	    file.close();
	}
	else cout << "not good" << endl;
    }

    void klasa()
    {
	int klasa;
	cout << "Choose the class of your character : " << endl;
	cout << "1 - Mage" << endl << "2 - Warrior" << endl << "3 - Berserker" << endl << "4 - Thief" << endl;
	cin >> klasa;
	switch (klasa)
	{
	case 1:
	    mage m;
	    m.stat(i);
	    i = m.i;
	    break;
	case 2:
	    warrior w;
	    w.stat(e);
	    e = w.e;
	    break;
	case 3:
	    berserker b;
	    b.stat(s);
	    s = b.s;
	    break;
	case 4:
	    thief t;
	    t.stat(d);
	    d = t.d;
	    break;
	default:
	    cout << "Wrong number" << endl;
	    break;

	}

    }


};

class monsters : character
{

public:
    monsters(string name="teamA")
    {
	n = name;



	for (int j = 0; j < 5; j++)
	{
	    s = rand() % 10 + 1;
	    d = rand() % 10 + 1;
	    e = rand() % 10 + 1;
	    i = rand() % 10 + 1;
	    c = rand() % 10 + 1;
	    character::save();
	}

	
    }

};


void load()
{
    int s, d, e, i, c;
    string name;

    cout << "What character do you want to load?" << endl;
    cin >> name;

    fstream file;
    file.open(name.c_str());
    if (file.good() == true)
    {
	file >> s >> d >> e >> i >> c;
	cout << "Loaded character '" << name << "' with the following statistics:" << endl;
	cout << "Strength : " << s << endl;
	cout << "Dexterity : " << d << endl;
	cout << "Endurance : " << e << endl;
	cout << "Intelligence : " << i << endl;
	cout << "Charisma : " << c << endl;
	file.close();
    }
    else cout << "not good" << endl;

}