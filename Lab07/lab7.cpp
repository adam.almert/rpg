#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

class character
{
private:
    int s, d, e, i, c;
    string n;
public:
    character(string name, int strength, int dexterity, int endurance, int intelligence, int charisma)
    {
	n = name;
	s = strength;
	d = dexterity;
	e = endurance;
	i = intelligence;
	c = charisma;

    }
    
    



    void save(string name, int s, int d, int e, int i, int c)
    {
	ofstream file;
	file.open(name.c_str());
	if (file.good() == true)
	{
	    file << s << " " << d << " " << e << " " << i << " " << c;
	    file.close();
	}
	else cout << "not good" << endl;
    }
};

void load()
{
    int s, d, e, i, c;
    string name;

    cout << "What character do you want to load?" << endl;
    cin >> name;

    fstream file;
    file.open(name.c_str());
    if (file.good() == true)
    {
	file >> s >> d >> e >> i >> c;
	cout << "Loaded character '" << name << "' with the following statistics:" << endl;
	cout << "Strength : " << s << endl;
	cout << "Dexterity : " << d << endl;
	cout << "Endurance : " << e << endl;
	cout << "Intelligence : " << i << endl;
	cout << "Charisma : " << c << endl;
	file.close();
    }
    else cout << "not good" << endl;

}



int main()
{
    int choice, s, d, e, i, c;
    string n;

    do
    {
	cout << "What do you want to do?" << endl;
	cout << "1 - Save a new character" << endl;
	cout << "2 - Load a character" << endl;
	cout << "3 - Quit" << endl;
	cin >> choice;


	if (choice == 1)
	{	
	    cout << "Enter the name of your new character : ";
	    cin >> n;
	    cout << "Strength : ";
	    cin >> s;
	    cout << "Dexterity : ";
	    cin >> d;
	    cout << "Endurance : ";
	    cin >> e;
	    cout << "Intelligence : ";
	    cin >> i;
	    cout << "Charisma : ";
	    cin >> c;

	    character ch(n, s, d, e, i, c); 
	    ch.save(n, s, d, e, i, c);
	}
	else if (choice == 2)
	{
	    load();
	}
	else if (choice == 3)
	{
	    break;
	}
	else cout << "Incorrect number!!";
	cout << "---------------------------------" << endl;
    } while (true);

    return 0;
}