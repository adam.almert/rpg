#ifndef class_hpp
#define class_hpp

class figure {
public:
virtual double area()=0;
virtual double circumference()=0;
virtual ~figure(){};

};
#endif