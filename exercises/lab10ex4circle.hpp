#ifndef circle_hpp
#define circle_hpp

#include "lab10ex4class.hpp"

class circle : public figure {
public:
circle(double);
~circle();
double area();
double circumference();
private:
double radius;
};

#endif