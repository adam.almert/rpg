#ifndef rectangle_hpp
#define rectangle_hpp

#include "lab10ex4class.hpp"

class rectangle : public figure {
public:
rectangle(double, double);
~rectangle();
double area();
double circumference();
private:
double side_a, side_b;
};
#endif
