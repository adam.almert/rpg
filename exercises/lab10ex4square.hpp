#ifndef square_hpp
#define square_hpp
#include "lab10ex4class.hpp"

class square : public figure {
public:
square(double);
~square();
double area();
double circumference();
private:
double side_a;
};

#endif
