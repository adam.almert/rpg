#include <iostream>
#include "lab10ex4circle.hpp"
const float pi = 3.14159;

circle::circle(double r){
radius=r;
}
circle::~circle(){
std::cout <<"circle destroyed"<<std::endl;
}
double circle::area(){
return (pi*radius*radius);
}
double circle::circumference(){
return (pi*radius*2);
}
